package com.campingo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TunisiaDisco1Application {

	public static void main(String[] args) {
		SpringApplication.run(TunisiaDisco1Application.class, args);
	}

}
