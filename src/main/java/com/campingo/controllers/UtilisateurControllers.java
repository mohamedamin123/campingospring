package com.campingo.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.campingo.entites.Utilisateur;
import com.campingo.request.nometprenomrequest;
import com.campingo.service.UtilisateurService;
import com.campingo.service.UtilisateurServiceImp;


@RestController
@RequestMapping("/user")
public class UtilisateurControllers {
	@Autowired
	private UtilisateurService utilisateurService;
@Autowired
private UtilisateurServiceImp utilisateurServiceImp;
	@GetMapping // http://localhost:8080/utilisateur
	public List<Utilisateur> getAllUtilisateur() {
		return utilisateurService.getAllUtilisateur();
	}

	@GetMapping(path = "/{id}") // localhost:8080/utilisateur/1
	public ResponseEntity<Utilisateur> findUserById(@PathVariable Long id) {
		Utilisateur utilisateur = utilisateurService.findUserById(id);

		if (utilisateur == null) {
			return new ResponseEntity<Utilisateur>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Utilisateur>(utilisateur, HttpStatus.OK);
		}
	}

	@PostMapping
	public Utilisateur createUtilisateur(@RequestBody Utilisateur utilisateur) {
		return utilisateurService.createUtilisateur(utilisateur);
	}

	@PutMapping
	public Utilisateur updateUtilisateur(@RequestBody Utilisateur utilisateur) {
		return utilisateurService.updateUtilisateur(utilisateur);
	}

	@DeleteMapping(path = "/{id}")
	public void deleteUtilisateur(@PathVariable Long id) {
		utilisateurService.deleteUtilisateur(id);
	}

	// localhost:8080/utilisateur/findUserByNom/admin
	@GetMapping(path = "/findbynom/{nom}")
	public ResponseEntity<List<Utilisateur>> findUserByNom(@PathVariable String nom) {
		List<Utilisateur> utilisateurs = utilisateurService.findUserByNom(nom);

		if (utilisateurs .isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}

	// localhost:8080/utilisateur/findUserByNom/nom/prenom
	@GetMapping(path = "/findbynometprenom/{nom}/{prenom}")
	public ResponseEntity<List<Utilisateur>> findByNomAndPrenom(@PathVariable String nom, @PathVariable String prenom) {
		List<Utilisateur> utilisateurs = utilisateurService.findByNomAndPrenom(nom, prenom);

		if (utilisateurs .isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}
	// localhost:8080/utilisateur/findbynometprenomrb
		@GetMapping(path = "/findbynometprenomrb")
		public ResponseEntity<List<Utilisateur>> findByNomAndPrenom(@RequestBody nometprenomrequest nometprenomrequest) {
			List<Utilisateur> utilisateurs = utilisateurService.findByNomAndPrenom(nometprenomrequest.getNom(),nometprenomrequest.getPrenom());

			if (utilisateurs .isEmpty()) {
				return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
			}
		}
	// localhost:8080/utilisateur/findbyville/tn
		@GetMapping(path = "/findbyville/{ville}")
		public ResponseEntity<List<Utilisateur>> findByVille(@PathVariable String ville) {
			List<Utilisateur> utilisateurs = utilisateurService.findByVille(ville);

			if (utilisateurs .isEmpty()) {
				return new ResponseEntity<List<Utilisateur>>(utilisateurs,HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
			}
		}
	
	/*
	 * @PostMapping(path="/login") public User loginUser(@RequestBody User
	 * user)throws Exception { String email = user.getEmail(); String password =
	 * user.getPassword(); User userobj =null; if (email!=null && password!=null ) {
	 * userobj=userserv.findByEmailAndPassword(email, password); } if(userobj==null)
	 * { throw new Exception("bad requestt"); } return userobj; }
	 */
}
