package com.campingo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.campingo.entites.Utilisateur;



public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
	
	public List<Utilisateur> findUserByNom(String nom);
	public List<Utilisateur> findByNomOrPrenom(String nom, String prenom);
	public Utilisateur findByEmailAndPassword(String email, String password);
	public List<Utilisateur> findByVille(String ville);
	public List<Utilisateur> findByRoles(String role);
	public Utilisateur findByEmail(String email);

}
