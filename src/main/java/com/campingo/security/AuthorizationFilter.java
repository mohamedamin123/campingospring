package com.campingo.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {

	public AuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);

	}

	/*
	 * Cette méthode intercepte les requetes en entrée, puis elle va vérifier
	 * l'entete d'authorisation en vérifiant si le mot clé Bearer est présent ou non
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String header = request.getHeader(SecurityConstants.AUTORIZATION);

		if (header == null || !header.startsWith(SecurityConstants.BEARER)) {
			chain.doFilter(request, response);
			return;
		}

		UsernamePasswordAuthenticationToken authenticationToken = getAuth(request);
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
		chain.doFilter(request, response);

	}

	private UsernamePasswordAuthenticationToken getAuth(HttpServletRequest request) {
		String token = request.getHeader(SecurityConstants.AUTORIZATION);
		if (token != null) {
			// Ligne de code incorrecte durant la formation
			// Ici je voulais tout simplement supprimmer le mot clé Bearer du token
			token = token.replace(token, SecurityConstants.BEARER);
			
			// Voici la ligne de code correcte qui permet 
			token = token.replace(SecurityConstants.BEARER,"");
			
			String user = Jwts.parser().setSigningKey(SecurityConstants.SECRET_JWT)
					.parseClaimsJws(token).getBody()
					.getSubject();

			if (user != null) {
				return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());

			}
			return null;

		}
		return null;

	}
}
