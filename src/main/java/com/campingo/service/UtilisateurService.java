package com.campingo.service;

import java.util.List;


import org.springframework.security.core.userdetails.UserDetailsService;

import com.campingo.entites.Utilisateur;



public interface UtilisateurService extends UserDetailsService {
	//methode CRUD
	public List<Utilisateur> getAllUtilisateur();
	public Utilisateur findUserById(Long id);
	public Utilisateur createUtilisateur(Utilisateur utilisateur);
	public Utilisateur updateUtilisateur(Utilisateur utilisateur);
	public void deleteUtilisateur(Long id);
	
	//methode avances
	public List<Utilisateur> findUserByNom(String nom);
	public List<Utilisateur> findByNomAndPrenom(String nom , String prenom);
	public Utilisateur findByEmailAndPassword(String email , String password);
	public Utilisateur findByEmail(String email);
	public List<Utilisateur> findByVille(String ville);
	public List<Utilisateur> findByRolesTitre(String titre);
	
}
