package com.campingo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.campingo.entites.Role;
import com.campingo.entites.Utilisateur;
import com.campingo.repository.UtilisateurRepository;



@Service
public class UtilisateurServiceImp implements UtilisateurService {

	@Autowired
	private UtilisateurRepository utilisateurRespository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public List<Utilisateur> getAllUtilisateur() {

		return utilisateurRespository.findAll();
	}

	public Utilisateur findUserById(Long id) {
		Optional<Utilisateur> usoptional = utilisateurRespository.findById(id);

		if (usoptional.isEmpty()) {
			return null;
		} else {
			return usoptional.get();
		}

	}

	@Override
	public Utilisateur createUtilisateur(Utilisateur utilisateur) {
		String cryptedPassword = bCryptPasswordEncoder.encode(utilisateur.getPassword());
		utilisateur.setPassword(cryptedPassword);
		return utilisateurRespository.save(utilisateur);
	}

	@Override
	public Utilisateur updateUtilisateur(Utilisateur utilisateur) {

		Optional<Utilisateur> utoptional = utilisateurRespository.findById(utilisateur.getId());
		if (utoptional.isEmpty()) {
			return null;
		} else {
			String cryptedPassword = bCryptPasswordEncoder.encode(utilisateur.getPassword());
			utilisateur.setPassword(cryptedPassword);
			return utilisateurRespository.save(utilisateur);
		}
	}

	@Override
	public void deleteUtilisateur(Long id) {
		utilisateurRespository.deleteById(id);

	}

	@Override
	public List<Utilisateur> findUserByNom(String nom) {
		return utilisateurRespository.findUserByNom(nom);
	}

	@Override
	public List<Utilisateur> findByNomAndPrenom(String nom, String prenom) {
		
		return utilisateurRespository.findByNomOrPrenom(nom, prenom);
	}

	@Override
	public Utilisateur findByEmailAndPassword(String email, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Utilisateur> findByVille(String ville) {
		return utilisateurRespository.findByVille(ville);
	}

	

	@Override
	public Utilisateur findByEmail(String email) {
		// TODO Auto-generated method stub
		return utilisateurRespository.findByEmail(email);
	}

	@Override
	public List<Utilisateur> findByRolesTitre(String titre) {
	
		return utilisateurRespository.findByRoles(titre);
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
          Utilisateur utilisateur = utilisateurRespository.findByEmail(email);
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(Role r: utilisateur.getRoles()) {
			GrantedAuthority authority= new SimpleGrantedAuthority(r.getTitre());
			authorities.add(authority);
		}
		
		return new User(utilisateur.getEmail(),utilisateur.getPassword(),authorities);
	}
	

}
